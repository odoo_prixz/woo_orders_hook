{
    'name': 'Woo orders hooks',
    'version': '14.0.1.0.0',
    'category': 'stock',
    'summary': """Peticion de estado ordenes""",
    'description': """
        Se agrega el el controller para recibir peticiones http por método get para consultar el estado de los pedidos en woo
    """,
    'author': 'Prixz',
    'depends': ['shipping_type', 'woocommerce_odoo_connector'],
    'data': [
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
