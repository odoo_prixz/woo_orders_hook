# -*- coding: utf-8 -*-
import json
import logging

from odoo import http
from datetime import datetime, timedelta
from odoo.http import request
import odoo

_logger = logging.getLogger(__name__)


class WebHookController(http.Controller):

    def _get_tipe_shipping(self, logistic):
        if logistic.type == "Local":
            shipping = {
                    "shipping": "Repartidor Prixzer",
                    "type": logistic.type,
                    "name": logistic.delivery_man_name if logistic.delivery_man_name else "",
                    "last_name": logistic.delivery_man_app if logistic.delivery_man_app else "",
                    "surname": logistic.delivery_man_apm if logistic.delivery_man_apm else "",
                    "email": "",
                    "mobile_phone": "",
                    "evidence": {
                        "name": "Sin evidencia aún"
                    }
                }
        else:
            shipping = {
                    "shipping": "EnvioClick",
                    "type": logistic.type,
                    "courierName": logistic.carrier if logistic.carrier else "",
                    "OrderIdEnvioClick": logistic.trackingCode,
                    "tracker": logistic.url_tracking,
                    "guide_pdf": "" #por el momento vacio
                }
        return shipping

    def _get_products_to_processing(self,logistic_ids, order_id):
        picking_done_ids = logistic_ids.mapped("picking_id")
        picking_ids = order_id.env["stock.picking"].search([
            ("sale_id", "=", order_id.id),
            ('picking_type_code', '=', 'outgoing'),
            ('state', 'in', ['confirmed', 'assigned', 'waiting'])
        ])
        restan = picking_ids.filtered(lambda x: x.id not in picking_done_ids.ids)
        products = []
        for record in restan:
            for lines in record.move_ids_without_package:
                product_channel_id = self.get_mapping_woo(lines.product_id)
                products.append({
                    "product": lines.product_id.name,
                    "barcode": lines.product_id.barcode,
                    "product_id": product_channel_id,
                })
        return products

    def get_mapping_woo(self, product_id):
        product_channel_id = ""
        for mapping in product_id.channel_mapping_ids:
            if mapping.store_product_id:
                product_channel_id = mapping.store_product_id
                break
        return product_channel_id

    def _get_products(self, logistic):
        products = []
        for record in logistic.order_id.order_line.filtered(lambda x: x.line_warehouse_id == logistic.warehouse_id):
            if record.product_id.type == "product":
                product_channel_id = self.get_mapping_woo(record.product_id)
                products.append({
                    "product": record.product_id.name,
                    "barcode": record.product_id.barcode,
                    "product_id": product_channel_id,
                })
        return products

    def _get_data_json(self, logistic_ids):
        data = []
        for logistic in logistic_ids:
            fecha_promesa = self.get_fecha_compromiso(logistic)
            incidence = "null"
            reassigned_at = None
            incidence_at = "null"
            canceled_at = "null"
            if logistic.incidence:
                if logistic.status in ["CANCEL_REQUEST", "CANCELED"]:
                    incidence = "canceled"
                    canceled_at = str(logistic.date_canceled - timedelta(hours=6)) if logistic.date_canceled else "null"
                    incidence_at = logistic.incidence
                else:
                    incidence = "incidence"
                    reassigned_at = str(logistic.date_accepted - timedelta(hours=6)) if logistic.date_accepted else None
                    canceled_at = "null"#str(logistic.date_canceled - timedelta(hours=6)) if logistic.date_canceled else "null"
                    incidence_at = "null"#logistic.incidence
            status = {
                    "unprocessed_at": str(logistic.order_id.create_date - timedelta(hours=6)),
                    "createdAt": str(logistic.order_id.date_order - timedelta(hours=6)) if logistic.order_id.date_order else "null",
                    "assigned_at": str(logistic.order_id.woo_date_paid - timedelta(hours=6)) if logistic.order_id.woo_date_paid else "null",
                    "incidence": incidence,
                    "incidence_at": [
                        incidence_at,
                    ],
                    "cancelled_at": canceled_at,
                    "reassigned_at": reassigned_at if reassigned_at else "null",
                    "in_situ_at": str(logistic.date_driver_in_branch - timedelta(hours=6)) if logistic.date_driver_in_branch else "null",
                    "recollected_at": str(logistic.date_driver_in_branch - timedelta(hours=6)) if logistic.date_driver_in_branch else "null",
                    "delivered_at": str(logistic.date_completed - timedelta(hours=6)) if logistic.date_completed else "null",
                }
            values = {
                "id": logistic.name,
                "order_id": logistic.order_id.name,
                "fecha_compromiso": str(fecha_promesa - timedelta(hours=6)) if fecha_promesa else "",
                "status": status,
                "shipping": self._get_tipe_shipping(logistic),
                "products": self._get_products(logistic),
                "at_status": self._get_last_status(status)
            }
            data.append(values)
        return data

    def _get_last_status(self, status):
        key_before = 'createdAt'
        tipo_incidencia = status.get("incidence")
        cancelled_at = status.get("cancelled_at")
        if status.get("delivered_at") != "null" and cancelled_at == "null":
            return "delivered_at"
        for key, value in status.items():
            if value == "null" and key not in ["incidence", "incidence_at"]:
                if key == "cancelled_at":
                    if tipo_incidencia == "canceled":
                        key_before = "canceled_at"
                    elif cancelled_at == "null":
                        continue
                if key == "reassigned_at":
                    continue
                break
            elif key not in ["incidence", "incidence_at"]:
                key_before = key
        return key_before

    def get_products_json_processing(self, logistic_ids, woo_order):
        order_id = request.env["sale.order"].sudo().search([
            ("name", "=", woo_order)
        ], limit=1)
        if order_id:
            products = self._get_products_to_processing(logistic_ids, order_id)
            if not products:
                return False
            values = {
                "id": "null",
                "order_id": order_id.name,
                "fecha_compromiso": "",
                "status": {
                    "unprocessed_at": str(order_id.create_date - timedelta(hours=6)),
                    "createdAt": str(order_id.date_order - timedelta(
                        hours=6)) if order_id.date_order else "null",
                    "assigned_at": "null",
                    "incidence": "null",
                    "incidence_at": [
                        "null",
                    ],
                    "cancelled_at": "null",  # por el momento fijo,
                    "reassigned_at": "null",
                    "in_situ_at": "null",
                    "recollected_at": "null",
                    "delivered_at": "null",
                },
                "shipping": {
                    "shipping": "Repartidor Prixzer",
                    "type": "Local",
                    "name": "",
                    "last_name": "",
                    "surname": "",
                    "email": "",
                    "mobile_phone": "",
                    "evidence": {
                        "name": "Sin evidencia aún"
                    }
                },
                "products": products,
                "at_status": "createdAt",
            }
            return values

    def get_fecha_compromiso(self, logistic_id):
        for line in logistic_id.order_id.order_line.filtered(lambda x: x.line_warehouse_id == logistic_id.warehouse_id):
            if line.fecha_promesa:
                return line.fecha_promesa
        return False

    @http.route('/web/orders/<string:woo_order>', type='http', methods=['GET'], auth='public', save_session=False)
    def webhook_orders_listener(self, **kwargs):
        if kwargs.get("woo_order"):
            woo_order = kwargs.get("woo_order")
            woo_order = woo_order.split("woo_")[1]
            logistic_ids = request.env['jelp.pedidos'].sudo().search([
                ("order_id", "=", woo_order)
            ])
            data = self._get_data_json(logistic_ids)
            val_pendiente = self.get_products_json_processing(logistic_ids, woo_order)
            if val_pendiente:
                data.append(val_pendiente)
            return json.dumps(data)

    @http.route('/web/orders/check', type='json', auth='public', save_session=False)
    def webhook_listener(self, **kwargs):
        json_request = request.jsonrequest
        orders = json_request.get("orders")
        sale_order = request.env['sale.order']
        available = []
        not_available = []
        not_available_to_update = []
        for record in orders:
            order = record.get('order_id')
            order_id = sale_order.sudo().search([
                ("name", "=", order)
            ])
            if order_id:
                available.append(order)
            else:
                not_available.append(order)
                not_available_to_update.append({
                    "order": order,
                    "status": record.get('status')
                })
        self.generate_order_not_avalibale_cron(not_available_to_update)
        response = {
            'available': available,
            'not_available': not_available,
            'total_order': len(available) + len(not_available),
            'total_available': len(available),
            'total_not_available': len(not_available)
        }
        return response

    def generate_order_not_avalibale_cron(self, not_available_to_update, tipo=None, body=None):
        order_to_update = request.env(user=odoo.SUPERUSER_ID)['orders.to.update']
        for not_available in not_available_to_update:
            order = not_available.get("order")
            status = not_available.get("status")
            if not tipo:
                orders = order_to_update.search([
                    ("name", "=", order),
                    ("estado", "=", "to_update")
                ])
                if not orders:
                    vals = {
                        "name": order,
                        "estado_woo": status,
                        "estado": "to_update",
                        "body": "Desde cron",
                    }
                    order_to_update.create(vals)
                    request.cr.commit()
            else:
                orders = order_to_update.search([
                    ("name", "=", order),
                    ("estado", "=", "to_update"),
                    ("tipo", "=", tipo)
                ])
                if not orders:
                    vals = {
                        "name": order,
                        "estado_woo": status,
                        "estado": "to_update",
                        "tipo": tipo,
                        "body": body,
                    }
                    order_to_update.create(vals)
                    request.cr.commit()
